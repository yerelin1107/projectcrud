import React from "react"
import {TableContainer,TableRow,TableHead,TableCell,TableBody} from '@material-ui/core'
import usePosts from "../hooks/usePost"
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import CreateIcon from '@material-ui/icons/Create';

const Crud = () => {

 
  const page = usePosts()
  const rows =  page.allStrapiTableUser.nodes
  
    return(
        <div className= "tabla">
   <TableContainer>
     <table>
     <TableHead>
       <TableRow>
         <TableCell>
          {" "}
          <strong>Nombre de usuario</strong>
          </TableCell>
          <TableCell>
            {" "}
           <strong>Acciones</strong>
          </TableCell>
       </TableRow>
     </TableHead>
   <TableBody>
          {rows.map((item) => (
            <TableRow key={item.id}>
              <TableCell align="left">
                {item.username}
                </TableCell>
              <TableCell>
              
                <AddIcon></AddIcon>
                <CreateIcon color="primary"></CreateIcon>
                <DeleteIcon color="secondary"></DeleteIcon>
                </TableCell>
            </TableRow>
            
          ))}
        </TableBody>
        </table>
        </TableContainer>
   </div>
    )
}


 
export default Crud;