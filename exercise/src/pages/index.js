import React from "react"
import Layout from "../components/layout"
import Crud from "../components/crud"
import AppBar  from "../components/AppBar"


const IndexPage = () => (
  <section>
  <AppBar/>
  <Layout>
  <h1>CRUD</h1>
  <Crud></Crud>
  </Layout>
  </section>
)

export default IndexPage
